module.exports = {
	/*
	** Headers of the page
	*/
	head: {
		htmlAttrs: {
			lang: 'ru'
		},
		title: 'hb',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: 'Nuxt.js project' }
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
		]
	},
	/*
	** Customize the progress bar color
	*/
	loading: {
		color: '#774098',
		height: '5px'
	},
	cache: true,
	/*
	** Build configuration
	*/
	build: {
		extractCSS: false,
		/*
		** Run ESLint on save
		*/
		extend(config, { isDev, isClient }) {
			if (isDev && isClient) {
				config.module.rules.push({
					enforce: 'pre',
					test: /\.(js|vue)$/,
					loader: 'eslint-loader',
					exclude: /(node_modules)/
				});
			}
		},
		postcss: [
			require('postcss')(),
			require('postcss-image-inliner')(),
			require('postcss-inline-svg')({
				path: '@/images/',
				removeFill: true
			}),
			require('cssnano')()
		],
		vendor: ['axios', 'lazyload', 'tabs', 'swiper']
	},
	css: [
		{
			src: '~assets/styles/scss/style.scss', lang: 'scss'
		}
	],
	plugins: [
		'~plugins/lazyload.js',
		'~plugins/tabs.js',
		'~plugins/swiper.js'
	],
	generate: {
		routes: function () {
			return [
				'/news/1',
				'/news/2'
			];
		}
	},
	router: {
		linkActiveClass: 'active',
		extendRoutes(routes, resolve) {
			routes.push({
				name: 'custom',
				path: '*',
				component: resolve(__dirname, 'pages/404.vue')
			});
		}
	},
	env: {
		browser: true
	}
};
